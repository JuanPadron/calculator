﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormCalculator
{
    public partial class Form1 : Form
    {
        string number1;
        string number2;
        char oper = ' ';
        float result;
        bool valResult = false;
        
        public Form1()
        {
            InitializeComponent();
        }
        

        private void setDisplay()
        {
            textBox1.Text = number1 + oper + number2;
        }


        private void buttonOper_Click(object sender, EventArgs e)
        {
            oper = Char.Parse((sender as Button).Text);
            setDisplay();
            valResult = false;
        }
        
        
        private void buttonEqual_Click(object sender, EventArgs e)
        {
            float int1 = float.Parse(number1);
            float int2 = float.Parse(number2);    

            if (oper == '+')
            {
                result = int1 + int2;
                number1 = "" + result;
                oper = ' ';
                number2 = null;
                setDisplay();
                valResult = true;
            }
            else if (oper == '-')
            {
                result = int1 - int2;
                number1 = "" + result;
                oper = ' ';
                number2 = null;
                setDisplay();
                valResult = true;
            }
            else if (oper == 'x')
            {
                result = int1 * int2;
                number1 = "" + result;
                oper = ' ';
                number2 = null;
                setDisplay();
                valResult = true;
            }
            else if (oper == '/')
            {
                if (int2 != 0)
                {
                    result = int1 / int2;
                    number1 = "" + result;
                    oper = ' ';
                    number2 = null;
                    setDisplay();
                    valResult = true;
                }
                else if (int2 == 0)
                {
                    textBox1.Text = "Cannot / by 0";
                }
            }
            else if (oper == '^')
            {
                result = int1 * int1;
                number1 = "" + result;
                oper = ' ';
                number2 = " ";
                setDisplay();
                valResult = true;
            }
        }


        private void buttonNum_Click(object sender, EventArgs e)
        {
            if (valResult == true)
            {
                number2 = null;
                oper = ' ';
                number1 = null;
                textBox1.Clear();
                valResult = false;
                if (oper != ' ')
                {
                    number2 += (sender as Button).Text;
                }
                else
                {
                    number1 += (sender as Button).Text;
                }
                setDisplay();
            }
            else
            {
                if (oper != ' ')
                {
                    number2 += (sender as Button).Text;
                }
                else
                {
                    number1 += (sender as Button).Text;
                }
                setDisplay();
            }
        }


        private void buttonValue_Click(object sender, EventArgs e)
        {
            if (oper != ' ')
            {
                if (number2 != null)
                { 
                    if (number2.Contains('-'))
                    {
                        number2 = number2.Substring(1, number2.Length-1);
                    }
                    else
                    {
                        number2 = number2.Insert(0, "-");
                    }
                }
            }
            else if (number1 != null)
            {
                if (number1.Contains('-'))
                {
                    number1 = number1.Substring(1, number1.Length - 1);
                }
                else
                {
                    number1 = number1.Insert(0, "-");
                }
            }
            setDisplay();
        }
        
        
        private void buttonDec_Click(object sender, EventArgs e)
        {
            if (number2 != null)
            {
                if (number2.Contains('.'))
                {
                    //do nothing
                }
                else
                {
                    number2 += ".";
                }
            }
            else if (number1 != null)
            {
                if (number1.Contains('.'))
                {
                    //d0 nothing
                }
                else
                {
                    number1 += ".";
                }
            }
            setDisplay();
        }
       
        
        private void ButtonSquare_Click(object sender, EventArgs e)
        {
            oper = '^';
            number2 = "2";
            setDisplay();
        }
        
        
        private void buttonC_Click(object sender, EventArgs e)
        {
            number2 = null;
            oper = ' ';
            number1 = null;
            textBox1.Clear();
        }
        
        
        private void buttonCE_Click(object sender, EventArgs e)
        {   //clear the recent entry on a calcultor
            if (number2 != null)
            {
                number2 = null;
                setDisplay();
            }
            else if (oper != ' ' )
            {
                oper = ' ';
                setDisplay();
            }
            else if (number1 != null)
            {
                number1 = null;
                setDisplay();
            }
        }
    }
}
